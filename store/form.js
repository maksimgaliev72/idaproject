export const state = () => ({
  form: []
})

export const mutations = {
  updateField (localState, { value, action }) {
    localState.form = localStorage.getItem('form') ? JSON.parse(localStorage.getItem('form')) : []
    if (action === 'add') {
      localState.form.push(value)
    } else if (action === 'remove') {
      const index = localState.form.findIndex(item => item.id === value)
      localState.form.splice(index, 1)
    }
    localStorage.setItem('form', JSON.stringify(localState.form))
  }
}

export const actions = {
  setForm ({ commit }, form) {
    commit('updateField', { value: form, action: 'add' })
  },
  removeItem ({ commit }, formId) {
    commit('updateField', { value: formId, action: 'remove' })
  }
}
